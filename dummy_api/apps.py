from django.apps import AppConfig


class DummyAPIConfig(AppConfig):
    name = 'dummy_api'
