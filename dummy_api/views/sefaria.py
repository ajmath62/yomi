import json

from django.http import JsonResponse


def text(request, ref):
    # Sample data from https://www.sefaria.org/api/texts/Kohelet.5
    with open('dummy_api/samples/sefaria-texts-kohelet.5.json') as fn:
        data = json.load(fn)
    return JsonResponse(data)
