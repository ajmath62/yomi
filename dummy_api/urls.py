from django.urls import path

from dummy_api.views import sefaria as sefaria_views

urlpatterns = [  # pylint: disable=invalid-name
    path('sefaria/texts/<str:ref>/', sefaria_views.text, name='sefaria-text'),
]
app_name = 'dummy_api'
