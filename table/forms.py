""" Forms for `table` app """
from django import forms

from table.constants import TaskStatus
from table.models import Task


class TaskInstanceForm(forms.Form):
    """ Form for updating a TaskInstance """
    date = forms.DateField()
    status = forms.ChoiceField(choices=TaskStatus.choices)
    task = forms.ModelChoiceField(queryset=Task.objects.all())
    
    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['task'].queryset = Task.objects.filter(user=user)
