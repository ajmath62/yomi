""" Helper functions """
import random
from datetime import timedelta

from django.utils import timezone

from table.constants import TaskStatus
from table.models import Task, TaskInstance


def get_date(user):
    """ Determine the date that a user is likely most interested in
    
    This will be the latest date with a TaskInstance, or the day after that if all Tasks from
    that day already have TaskInstances.
    """
    try:
        latest_task_date = (TaskInstance.objects.filter(task__user=user)
                            .values_list('date', flat=True).latest('date'))
    except TaskInstance.DoesNotExist:
        # The user has not filled out any task instances, so use today's date
        return timezone.localdate()
    total_tasks = Task.objects.filter(active=True, user=user).count()
    if TaskInstance.objects.filter(date=latest_task_date).count() == total_tasks:
        # All tasks for this day have been processed
        return latest_task_date + timedelta(days=1)
    else:
        return latest_task_date


def get_task_list(user, date):
    """ Get a sorted list of a user's tasks on a given day """
    tasks = list(Task.objects.filter(user=user, active=True).values('pk', 'name', 'url'))
    statuses = dict(
        TaskInstance.objects.filter(task__user=user, date=date).values_list('task_id', 'status')
    )
    for task in tasks:
        task['status'] = statuses.get(task['pk'], None)
    # Order the tasks randomly, with the answered tasks below the unanswered tasks
    # The randomness is seeded based on the current date and hour, so that the order does not
    # change with every task completion or page refresh.
    random.seed(timezone.now().isoformat(timespec='hours'))
    random.shuffle(tasks)
    # False < True, so status == None will come first
    tasks.sort(key=lambda t: t['status'] is not None)
    return tasks


def status_symbol(status):
    """ Determine the text to display on the frontend for a given status """
    if status == TaskStatus.success:
        return '⭕'
    elif status == TaskStatus.fail:
        return '❌'
    elif status == TaskStatus.pass_:
        return '–'
    else:
        # No status (e.g. no TaskInstance)
        return ''
