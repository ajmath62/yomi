from django.contrib import admin

from table import models


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user', 'name', 'url', 'active')


@admin.register(models.TaskInstance)
class TaskInstanceAdmin(admin.ModelAdmin):
    list_display = ('pk', 'task', 'date', 'status')
