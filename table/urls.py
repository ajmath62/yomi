""" URLs for `table` app """
from django.urls import path

from table import views

urlpatterns = [
    path('', views.view_tasks, name='table'),
    path('track', views.track, name='track'),
]
app_name = 'table'
