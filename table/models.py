""" Models for `table` app """
from django.contrib.auth.models import User
from django.db import models

from table.constants import TaskStatus


class Task(models.Model):
    """ Task to be performed """
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=128)
    # Making this a URLField doesn't allow deep links, so we use an unvalidated CharField
    url = models.CharField(blank=True, max_length=200)
    active = models.BooleanField(default=True)
    
    def __str__(self):
        return self.name


class TaskInstance(models.Model):
    """ Whether a task was performed on a given day """
    task = models.ForeignKey(Task, on_delete=models.PROTECT)
    date = models.DateField()
    status = models.CharField(max_length=32, choices=TaskStatus.choices)
    
    def __str__(self):
        if 'task' in self._state.fields_cache:
            # The query to fetch the task has already been made
            return f'{self.task} on {self.date}'
        else:
            return f'Task #{self.task_id} on {self.date}'
    
    class Meta:
        unique_together = ('task', 'date')
