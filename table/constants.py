""" Enums and constants """


class Choice(type):
    """ Metaclass that allows classes to generate Django choice tuples """
    @property
    def choices(cls):
        """ Generate tuples of the class's "normal" attributes """
        return [(v, v) for k, v in cls.__dict__.items()
                # k doesn't start with an underscore and v isn't a method
                if not k.startswith('_') and not hasattr(v, '__get__')]


class TaskStatus(metaclass=Choice):
    """ State for a particular TaskInstance """
    success = 'success'
    fail = 'fail'
    # `pass` is a reserved keyword
    pass_ = 'pass'
