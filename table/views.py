""" Views for `table` app """
import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils import timezone

from table import helpers
from table.forms import TaskInstanceForm
from table.models import TaskInstance


@login_required
def track(request):
    """ Allow a user to track their tasks """
    if request.method == 'POST':
        form = TaskInstanceForm(request.user, request.POST)
        if form.is_valid():
            task = form.cleaned_data['task']
            date = form.cleaned_data['date']
            task_instance, _ = TaskInstance.objects.get_or_create(date=date, task=task)
            task_instance.status = form.cleaned_data['status']
            task_instance.save()
            # Redirect to this same page with the same query parameters
            return redirect(request.get_full_path())
    
    user = request.user
    try:
        date = datetime.date.fromisoformat(request.GET.get('date', ''))
    except ValueError:
        # Empty or invalid parameter
        date = helpers.get_date(user)
    tasks = helpers.get_task_list(user, date)
    context = {
        'date': date,
        'tasks': tasks,
    }
    return render(request, 'table/tracking.html', context)


@login_required
def view_tasks(request):
    """ Show a user their completed tasks in table form """
    today = timezone.localdate()
    try:
        start_date = datetime.date.fromisoformat(request.GET.get('start', ''))
    except ValueError:
        # Empty or invalid parameter
        start_date = today - datetime.timedelta(days=30)
    try:
        end_date = datetime.date.fromisoformat(request.GET.get('end', ''))
    except ValueError:
        # Empty or invalid parameter
        end_date = today
    
    tasks = TaskInstance.objects.filter(
        date__gte=start_date,
        date__lte=end_date,
        task__user=request.user,
    ).values_list('task__name', 'date', 'status')
    name_list = list({name for name, _, _ in tasks})
    date_list = sorted({date for _, date, _ in tasks})
    status_mapping = {(name, date): status for name, date, status in tasks}
    task_display = [(name, [
        helpers.status_symbol(status_mapping.get((name, date), None)) for date in date_list
    ]) for name in name_list]
    context = {
        'dates': date_list,
        'end_date': end_date,
        'start_date': start_date,
        'tasks': task_display,
    }
    return render(request, 'table/table.html', context)
