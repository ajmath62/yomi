# Generated by Django 2.1.3 on 2018-11-25 23:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20181118_1925'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='sefaria_ref',
            field=models.CharField(blank=True, max_length=64),
        ),
    ]
