""" Tests for the Admin interface """
from io import StringIO

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from app.models import Project, Task


class ProjectAdminAppearanceTests(TestCase):
    def setUp(self):
        """ Create and log in as an admin user """
        self.username = 'aaron'
        self.password = 'supersecretpassword'
        User.objects.create_superuser(self.username, 'email', self.password)
        
        self.client.login(username=self.username, password=self.password)
        self.expected_input = '<input id="id_tasks" name="tasks" type="file">'
    
    def test_project_creation_has_input(self):
        """ Test that the Tasks input shows up on the creation page """
        # File field is present on the Project creation page
        url = reverse('admin:app_project_add')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertInHTML(self.expected_input, resp.content.decode(), count=1)
    
    def test_project_editing_has_input(self):
        """ Test that the Tasks input shows up on the editing page """
        # File field is present on the Project editing page if the project has no tasks
        project = Project.objects.create()
        url = reverse('admin:app_project_change', args=(project.pk,))
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertInHTML(self.expected_input, resp.content.decode(), count=1)
    
    def test_project_editing_doesnt_have_input(self):
        """ Test that the Tasks input doesn't show up on the editing page if there already tasks """
        # File field is not present if the project has tasks
        project = Project.objects.create()
        Task.objects.create(project=project, length=18, project_index=0)
        url = reverse('admin:app_project_change', args=(project.pk,))
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        # There is no 'assertNotInHTML', but asserting that it appears 0 times does the trick
        self.assertInHTML(self.expected_input, resp.content.decode(), count=0)


class TaskUploadTests(TestCase):
    def setUp(self):
        """ Create and log in as an admin user """
        self.username = 'aaron'
        self.password = 'supersecretpassword'
        User.objects.create_superuser(self.username, 'email', self.password)
        
        self.client.login(username=self.username, password=self.password)
        self.project = Project.objects.create()
        self.url = reverse('admin:app_project_change', args=(self.project.pk,))
        
        self.success_html = '<li class="success">'
    
    def test_uploading_tasks_on_project_creation(self):
        """ Test that tasks can be uploaded alongside project creation """
        add_url = reverse('admin:app_project_add')
        tasks = StringIO('task 0,18\ntask 1,26\ntask 2,54\n')
        expected_task_data = (
            ('task 0', 18, 0),
            ('task 1', 26, 1),
            ('task 2', 54, 2),
        )
        form_data = {
            'name': 'horatio',
            'tasks': tasks,
            'unit_name': 'doodad',
            'unit_plural': 'doodadz',
        }
        resp = self.client.post(add_url, form_data, follow=True)
        self.assertIn(self.success_html, resp.content.decode())
        
        # The three tasks should have been created in the correct order
        project = Project.objects.order_by('pk').last()
        self.assertEqual(project.tasks.count(), 3)
        # Use CountEqual because one is a queryset and the other is a tuple
        self.assertCountEqual(project.tasks.values_list('name', 'length', 'project_index'),
                         expected_task_data)
    
    def test_uploading_tasks_on_existing_project(self):
        """ Test that tasks can be added to a pre-existing project """
        tasks = StringIO('task 0,18\ntask 1,26\ntask 2,54\n')
        expected_task_data = (
            ('task 0', 18, self.project.pk, 0),
            ('task 1', 26, self.project.pk, 1),
            ('task 2', 54, self.project.pk, 2),
        )
        form_data = {
            'name': 'horatio',
            'tasks': tasks,
            'unit_name': 'doodad',
            'unit_plural': 'doodadz',
        }
        resp = self.client.post(self.url, form_data, follow=True)
        self.assertIn(self.success_html, resp.content.decode())
        
        # The three tasks should have been created in the correct order
        self.assertEqual(self.project.tasks.count(), 3)
        # Use CountEqual because one is a queryset and the other is a tuple
        self.assertCountEqual(
            self.project.tasks.values_list('name', 'length', 'project_id', 'project_index'),
            expected_task_data,
        )
    
    def test_uploading_more_tasks_on_project_should_fail(self):
        """ Test that tasks cannot be added to a project with other tasks on it """
        task = self.project.tasks.create(length=13, project_index=0)
        
        tasks = StringIO('task 0,18\ntask 1,26\ntask 2,54\n')
        form_data = {
            'name': 'horatio',
            'tasks': tasks,
            'unit_name': 'doodad',
            'unit_plural': 'doodadz',
        }
        resp = self.client.post(self.url, form_data, follow=True)
        self.assertIn(self.success_html, resp.content.decode())

        # No new tasks should have been created
        self.assertEqual(self.project.tasks.count(), 1)
        self.assertEqual(self.project.tasks.get(), task)
