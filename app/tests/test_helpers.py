""" Unit tests for the helper functions """
from django.test import SimpleTestCase

from app.helpers import calibrate_task_rate


class TaskRateTests(SimpleTestCase):
    def test_no_fewer_than_one_task_per_day(self):
        """ Test that the task rate is never smaller than one task per day """
        days = 10
        task_list = [('task', 3)]
        expected_task_rate = 1
        
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_no_tasks(self):
        """ Test that the task rate is 1 when there are no tasks remaining """
        days = 5
        task_list = []
        expected_task_rate = 1
        
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_no_days_remaining(self):
        """ Test that the task rate is the sum of the tasks when there are no days remaining """
        days = 0
        task_list = [('task', 3), ('task', 1)]
        expected_task_rate = 4
        
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_one_day_remaining(self):
        """ Test that the task rate is the sum of the tasks when there is one day remaining """
        days = 1
        task_list = [('task', 3), ('task', 106), ('task', 4)]
        expected_task_rate = 113
        
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_sample_values_1(self):
        """ Test that the task rate is correctly calibrated """
        days = 3
        task_list = [('task', 2), ('task', 5), ('task', 5), ('task', 18)]
        expected_task_rate = 3
    
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_sample_values_2(self):
        """ Test that the task rate is correctly calibrated """
        days = 2
        task_list = [('task', 2), ('task', 5), ('task', 5), ('task', 18)]
        expected_task_rate = 6
    
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_sample_values_3(self):
        """ Test that the task rate is correctly calibrated """
        days = 2
        task_list = [('task', 13), ('task', 5), ('task', 10), ('task', 4), ('task', 5)]
        expected_task_rate = 15
    
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_sample_values_4(self):
        """ Test that the task rate is correctly calibrated """
        days = 7
        task_list = [('task', 2), ('task', 2), ('task', 2), ('task', 2), ('task', 2), ('task', 2),
                     ('task', 2), ('task', 2)]
        expected_task_rate = 3
    
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
    
    def test_sample_values_5(self):
        """ Test that the task rate is correctly calibrated """
        days = 2
        task_list = [('task', 18), ('task', 2), ('task', 5), ('task', 2)]
        expected_task_rate = 8
    
        task_rate = calibrate_task_rate(days, task_list)
        self.assertEqual(task_rate, expected_task_rate)
