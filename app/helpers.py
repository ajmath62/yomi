from datetime import timedelta
from typing import Any, Dict, List, Tuple

from django.utils import timezone
from hebrew_numbers import int_to_gematria

from app.models import IndividualProject, Project, Task

FINISH_TEXT = 'Finish'


def get_schedule(project: IndividualProject) -> List[Dict[str, Any]]:
    today = timezone.localdate()
    days_left = (project.target - today).days + 1
    if days_left <= 0:
        return [{'date': today, 'name': 'Start again!', 'total': None}]
    
    raw_task_list = list(project.project.tasks.filter(
        project_index__gte=project.latest.project_index,
    ).values_list('name', 'length'))
    if not raw_task_list:
        return [{'date': today, 'name': 'Start again!', 'total': None}]
    if project.project.divisible:
        # A given day's task can end in the middle of a stored task, so for calculation purposes we
        # split the task into `length` pieces, each of length 1.
        # The first task is different, because it can start in the middle of a task
        name, length = raw_task_list.pop(0)
        task_list = [(task_plus_progress(name, i), 1) for i in range(length) if i >= project.latest_progress]
        for name, length in raw_task_list:
            task_list.extend((task_plus_progress(name, i), 1) for i in range(length))
    else:
        task_list = raw_task_list
    task_rate = calibrate_task_rate(days_left, task_list)
    
    schedule = []
    current_date = today
    day_length = 0
    for idx, (_, length) in enumerate(task_list):
        day_length += length
        if day_length >= task_rate:
            if idx + 1 >= len(task_list):
                next_name = FINISH_TEXT
            else:
                next_name = task_list[idx + 1][0]
            schedule.append({'date': current_date, 'name': next_name, 'total': day_length})
            current_date += timedelta(days=1)
            day_length = 0

    # If we haven't yet, add the task to complete the last tasks
    if schedule == [] or schedule[-1]['name'] != FINISH_TEXT:
        schedule.append({'date': current_date, 'name': FINISH_TEXT, 'total': day_length})
        current_date += timedelta(days=1)
    
    return schedule


def get_task_choices(project_id, starting_from, length=None, progress=0):
    """ Get a list of tasks for a given project
    
    :param project_id: The primary key of the project
    :param starting_from: The project_index of the first task to be included
    :param length: How many tasks to include in the return list. If not specified, all tasks
        through the end will be included.
    :param progress: For divisible projects, where in the task to start. Will be ignored for
        projects with divisible=False.
    :return: An iterable of tuples (value, text) for use in a ChoiceField
    """
    all_tasks = Task.objects.filter(project_id=project_id, project_index__gte=starting_from)
    if length is not None:
        # Limit the tasks to `length` tasks. For a divisible project, not all of these will be
        # included, but it is an upper bound.
        all_tasks = all_tasks[:length]
    all_tasks = list(all_tasks)
    
    divisible = Project.objects.values_list('divisible', flat=True).get(pk=project_id)
    if divisible:
        # The first task is different, because we only include subtasks from progress and up
        first_task = all_tasks.pop(0)
        return_value = [(f'{first_task.project_index}--{i}', task_plus_progress(first_task.name, i)) for i in range(first_task.length) if i >= progress]
        for task in all_tasks:
            return_value.extend((f'{task.project_index}--{i}', task_plus_progress(task.name, i)) for i in range(task.length))
        
        if length is not None:
            return_value = return_value[:length]
        return return_value
    else:
        return [(f'{e.project_index}--0', e.name) for e in all_tasks]


def calibrate_task_rate(days: int, task_list: List[Tuple[str, int]]) -> int:
    """ Determine a daily task rate for a list of tasks
    
    Given a list of tasks, determine the daily rate at which they must be consumed in order to be
    finished in a given number of days.
    """
    task_rate = 1  # lower bound of one task per day
    searching = True
    sum_of_tasks = sum(t[1] for t in task_list)
    
    if days <= 1:
        # There are one or fewer days remaining, so everything must be done today
        return sum_of_tasks
    
    while searching:
        day_length = 0
        days_passed = 0
        for _, length in task_list:
            day_length += length
            if day_length >= task_rate:
                days_passed += 1
                day_length = 0
        if day_length > 0:
            days_passed += 1
        
        if days_passed > days:
            task_rate += 1
        else:
            # We have found a sufficiently quick task rate, so we are done
            searching = False
    
    return task_rate


def task_plus_progress(task, progress) -> str:
    """ Combine a task and a progress to generate the task's full name
    
    Progress should be 0-indexed
    """
    progress_hebrew = int_to_gematria(progress + 1)
    return f'{task} {progress_hebrew}'
