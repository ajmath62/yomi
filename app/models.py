from django.contrib.auth.models import User
from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True)
    divisible = models.BooleanField(default=False)
    unit_name = models.CharField(max_length=20)
    unit_plural = models.CharField(max_length=20)
    
    def __str__(self):
        return self.name


class Task(models.Model):
    name = models.CharField(max_length=64)
    length = models.IntegerField()
    project = models.ForeignKey(Project, models.PROTECT, related_name='tasks')
    project_index = models.IntegerField()
    sefaria_ref = models.CharField(blank=True, max_length=64)
    
    class Meta:
        ordering = ('-project', 'project_index',)
        unique_together = ('project', 'project_index',)
    
    def __str__(self):
        return self.name


class IndividualProject(models.Model):
    user = models.ForeignKey(User, models.PROTECT, related_name='projects')
    project = models.ForeignKey(Project, models.PROTECT)
    target = models.DateField()
    latest = models.ForeignKey(Task, models.PROTECT)
    latest_progress = models.IntegerField(default=0)
    
    @property
    def full_latest(self) -> str:
        """ Get a full representation of the current state of the project """
        if self.project.divisible:
            return f'{self.latest.name} {self.latest_progress + 1}'
        else:
            return self.latest.name
    
    def __str__(self):
        return f'{self.project} {self.user_id}'
