from django.contrib.auth.models import User
from django.core.mail import EmailMessage, get_connection
from django.core.management.base import BaseCommand

from app.helpers import get_schedule


class Command(BaseCommand):
    help = 'Send email to all users'

    def handle(self, *args, **options):
        email_list = []
        for user in User.objects.all():
            subject = 'Your learning for today'
            messages = []
            for project in user.projects.order_by('pk').select_related('latest__project'):
                schedule = get_schedule(project)
                if schedule:
                    next_task = schedule[0]
                    if next_task['total'] is None:
                        messages.append(f'For {project.project.name}: {next_task["name"]}')
                    else:
                        messages.append(f'For {project.project.name}: '
                                        f'Learn up to {next_task["name"]}')
        
            messages.append('Keep up the good work!')
            email = EmailMessage(
                # language=HTML
                body='<br/>'.join(messages),
                from_email='ajmath62+x-yomi@gmail.com',
                subject=subject,
                to=(user.email,),
            )
            # This can't be done in __init__
            email.content_subtype = 'html'
            email_list.append(email)
        
        with get_connection() as conn:
            conn.send_messages(email_list)
