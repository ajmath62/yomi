from django.contrib import admin

from app import models
from app.forms import ProjectAdminForm
from app.models import Task


@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    form = ProjectAdminForm
    list_display = ('pk', 'name', 'description', 'divisible', 'tasks',)
    
    def get_fields(self, request, obj=None):
        base_fields = ('name', 'description', 'unit_name', 'unit_plural', 'divisible',)
        if obj is not None and obj.tasks.exists():
            # If the project exists and has tasks, don't allow uploading new tasks
            return base_fields
        else:
            return base_fields + ('tasks',)
    
    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        
        # If there were any tasks specified in the form and the project did not have any tasks, add
        # the new tasks to the project.
        if not obj.tasks.exists():
            tasks = form.cleaned_data['tasks']
            if tasks is not None:
                for task in tasks:
                    task.project_id = obj.pk
                Task.objects.bulk_create(tasks)
    
    @staticmethod
    def tasks(instance):
        tasks = instance.tasks.values_list('name', flat=True)
        if tasks.exists():
            if tasks.count() > 10:
                return ', '.join(tasks[:9]) + ',..., ' + tasks.last()
            else:
                return ', '.join(tasks)
        else:
            return 'None'


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'length', 'project', 'project_index',)


@admin.register(models.IndividualProject)
class IndividualProjectAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user', 'project', 'latest', 'target',)
