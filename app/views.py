import json
import re

import requests
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone

from app.forms import EditProjectForm, ProjectForm
from app.helpers import get_schedule
from app.models import IndividualProject, Task


# AJK TODO add a non-logged in landing page and actions
@login_required
def index(request):
    user: User = request.user
    project_list = [(project, get_schedule(project)[0]) for project in user.projects.all()]
    context = {
        'project_list': project_list,
    }
    return render(request, 'index.html', context)


@login_required
def project_details(request, project_id: int):
    project = get_object_or_404(IndividualProject, pk=project_id, user=request.user)
    showing_all = 'show-all' in request.GET
    
    if request.method == 'POST':
        form = EditProjectForm(request.POST, instance=project, showing_all=showing_all)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = EditProjectForm(instance=project, showing_all=showing_all)
    
    goal = get_schedule(project)[0]
    
    if project.project.divisible:
        # Link to the progress point in the next task
        latest_url = reverse('text', args=(project.pk, project.latest_id, project.latest_progress))
    else:
        # Link to the next task
        latest_url = reverse('text', args=(project.pk, project.latest_id))
    
    context = {
        'form': form,
        'goal': goal,
        'latest': latest_url,
        'project': project,
        'showing_all': showing_all,
    }
    return render(request, 'project-details.html', context)


@login_required
def project_timetable(request, project_id: int):
    project = get_object_or_404(IndividualProject, pk=project_id, user=request.user)
    schedule = get_schedule(project)
    context = {
        'project': project,
        'schedule': schedule,
    }
    return render(request, 'project-timetable.html', context)


@login_required
def text(request, project_id: int, task_id: int, progress=None):
    """ Display the text of a particular task
    
    This gets the text from Sefaria on the fly, so it doesn't work locally without internet access
    """
    task = get_object_or_404(Task, pk=task_id)
    # If the task has a known Sefaria ref, use that; if not, use the name.
    task_name = task.sefaria_ref or task.name
    if settings.USE_LOCAL_API:
        # http://localhost:PORT/local-dummy-url
        local_url = reverse('dummy:sefaria-text', args=(task_name,))
        sefaria_url = f'http://{request.META["HTTP_HOST"]}{local_url}'
    else:
        sefaria_url = f'https://www.sefaria.org/api/texts/{task_name}'
    
    # TODO: move this to the frontend
    # TODO: error handling
    response = requests.get(sefaria_url)
    if response.history:
        # There were redirects, so we replace the existing ref with the ref we got redirected to
        redirect_url = response.history[-1].headers.get('Location', '')
        match = re.fullmatch(r'^/api/texts/(.*)$', redirect_url)
        if match is not None:
            new_ref = match.group(1)
            task_name = task.sefaria_ref = new_ref
            task.save()
    
    content = json.loads(requests.get(sefaria_url).content)
    lines = content['he']
    
    # Determine the previous and next task or progress point within a task
    next_task = Task.objects.filter(
        project_id=task.project_id, project_index=task.project_index + 1,
    ).first()  # object or None
    prev_task = Task.objects.filter(
        project_id=task.project_id, project_index=task.project_index - 1,
    ).first()  # object or None
    if progress is None:
        # Display all lines and link to neighboring tasks
        display_text = lines
        if next_task is not None:
            next_task_url = reverse('text', args=(project_id, next_task.pk))
        else:
            next_task_url = None
        if prev_task is not None:
            prev_task_url = reverse('text', args=(project_id, prev_task.pk))
        else:
            prev_task_url = None
    else:
        # Only display the progress'th line and link to neighboring progress points
        display_text = [lines[progress]]
        if progress < len(lines) - 1:
            # Next progress point in this task
            next_task_url = reverse('text', args=(project_id, task_id, progress + 1))
        elif next_task is not None:
            # First progress point in the next task
            next_task_url = reverse('text', args=(project_id, next_task.pk, 0))
        else:
            # This is the last task in the project
            next_task_url = None
        
        if progress > 0:
            # Previous progress point in this task
            prev_task_url = reverse('text', args=(project_id, task_id, progress - 1))
        elif prev_task is not None:
            # Last progress point in the previous task
            prev_task_url = reverse('text', args=(project_id, prev_task.pk, prev_task.length - 1))
        else:
            # This is the first task in the project
            prev_task_url = None
    
    sefaria_link = f'https://sefaria.org/{task_name}'
    
    context = {
        'next': next_task_url,
        'previous': prev_task_url,
        'project_id': project_id,
        'sefaria_link': sefaria_link,
        'task': task,
        'text': display_text,
    }
    return render(request, 'text.html', context)


@login_required
def add_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            instance: IndividualProject = form.save(commit=False)
            instance.latest_id = (form.cleaned_data['project'].tasks
                                  .values_list('pk', flat=True).first())
            instance.user = request.user
            instance.save()
            return redirect('project-details', instance.pk)
    else:
        form = ProjectForm()
    
    form.fields['target'].initial = timezone.localdate()
    
    context = {
        'form': form,
    }
    return render(request, 'add-project.html', context)
