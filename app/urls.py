from django.urls import path

from app import views as app_views

urlpatterns = [  # pylint: disable=invalid-name
    path('', app_views.index, name='index'),
    path('project/', app_views.add_project, name='add-project'),
    path('project/<int:project_id>/', app_views.project_details, name='project-details'),
    path('project/<int:project_id>/timetable', app_views.project_timetable,
         name='project-timetable'),
    path('project/<int:project_id>/text/<int:task_id>/', app_views.text, name='text'),
    path('project/<int:project_id>/text/<int:task_id>/<int:progress>', app_views.text, name='text'),
]
