from typing import List

from django import forms
from django.core.exceptions import ValidationError

from app.helpers import get_task_choices
from app.models import IndividualProject, Project, Task


class EditProjectForm(forms.ModelForm):
    full_latest = forms.ChoiceField()
    target = forms.DateField(widget=forms.SelectDateWidget)
    
    def __init__(self, *args, showing_all, **kwargs):
        super().__init__(*args, **kwargs)
        
        latest_field = self.fields['full_latest']
        num_to_show = None if showing_all else 10
        choices = get_task_choices(self.instance.project_id, self.instance.latest.project_index, num_to_show, self.instance.latest_progress)
        latest_field.choices = choices
        latest_field.initial = self.instance.full_latest
    
    def clean_full_latest(self):
        """ Confirm that the provided value for the full_latest field is valid """
        index, progress = self.cleaned_data['full_latest'].split('--')
        try:
            task = Task.objects.get(project_id=self.instance.project_id, project_index=index)
        except Task.DoesNotExist:
            raise ValidationError(f'No task with project index {index} found.')
        
        return task, progress
    
    def save(self, commit=True):
        super().save(commit=False)
        
        # Parse full_latest into the latest and progress fields
        task, progress = self.cleaned_data['full_latest']
        self.instance.latest = task
        self.instance.latest_progress = progress
        self.instance.save()
    
    class Meta:
        fields = ('full_latest', 'target',)
        model = IndividualProject


class ProjectAdminForm(forms.ModelForm):
    tasks = forms.FileField(required=False)
    
    def clean_tasks(self) -> List[Task]:
        if self.cleaned_data['tasks'] is None:
            return []
        
        line: bytes
        task_list = []
        for i, line in enumerate(self.cleaned_data['tasks']):
            line_str = line.decode()
            name, length = line_str.split(',')
            task_list.append(Task(name=name, length=length, project_index=i))
        
        return task_list


class ProjectForm(forms.ModelForm):
    project = forms.ModelChoiceField(queryset=Project.objects.all())
    target = forms.DateField(widget=forms.SelectDateWidget)
    
    class Meta:
        fields = ('project', 'target',)
        model = IndividualProject
