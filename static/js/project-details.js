(function () {
  'use strict'
  
  window.editMode = false
  window.switchEditMode = switchEditMode
  
  ////////
  
  function switchEditMode() {
    if (window.editMode) {
      // Leaving edit mode, so show the display version
      $('#target-display').css('display', '')
      $('#target-edit').css('display', 'none')
      $('#target-edit-button').text('Edit')
      window.editMode = false
    } else {
      // Entering edit mode, so show the edit version
      $('#target-display').css('display', 'none')
      $('#target-edit').css('display', '')
      $('#target-edit-button').text('Cancel')
      window.editMode = true
    }
  }
})()
